#!/bin/bash

for filename in $(find obj/solution/ -name '*'); do
  if [ -f "$filename" ]; then
    rm $filename
  fi
done

for filename in $(find solution/src/ -name '*.c'); do
  name=$(basename $filename)
  name=$(echo $name | sed -e 's/.$//')
  gcc -c $filename -o obj/solution/${name}o
done

filenames=(obj/solution/*)
gcc -o build/image-transformer ${filenames[*]}

./build/image-transformer $@
