//
// Created by diana on 31.10.22.
//
#include "../include/bmp.h"

const char* reading_message[] = {
        [READ_OK]                     = "Image loaded successfully!\n",
        [READ_INVALID_SIGNATURE]      = "Error! Invalid signature\n",
        [READ_INVALID_BITS]           = "Error! Invalid bits\n",
        [READ_INVALID_HEADER]         = "Error! Invalid header\n"
};
const char* writing_message[] = {
        [WRITE_OK]      = "Image saved successfully!\n",
        [WRITE_ERROR]   = "Error! Image wasn't saved\n"
};

uint8_t padding(uint64_t width) {
    if ((width * 3) % 4 == 0) {
        return 0;
    } else {
        return 4 - (3 * width) % 4;
    }
}

struct bmp_header create_bmp_header(struct image const* img) {
    struct bmp_header header = {0};
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + img->height * (img->width * sizeof(struct pixel) +
                                                                  padding(img->width));
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biSizeImage = 0;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.bOffBits = 54;
    header.bfReserved = 0;
    return header;
}

enum read_status read_bmp_header(struct bmp_header* header, FILE* file) {
    fread(header, sizeof(struct bmp_header), 1, file);
    if (ferror(file)) {
        return READ_INVALID_HEADER;
    }
    if (header->bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biBitCount != 24) {
        return READ_INVALID_BITS;
    } else {
        return READ_OK;
    }
}

enum read_status from_bmp(FILE* in, struct image* img) {
    if (in == NULL) {
        return READ_INVALID_PATH;
    }
    struct bmp_header header;
    enum read_status status = read_bmp_header(&header, in);
    if (status != 0) {
        return status;
    }
    img->width = header.biWidth;
    img->height = header.biHeight;

    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
    for (uint64_t i = 0; i < img->height; i++) {
        fread(img->data + img->width * i, sizeof(struct pixel), img->width, in);
        fseek(in, padding(img->width), SEEK_CUR);
        if (ferror(in)) {
            return READ_ERROR;
        }
    }
    return READ_OK;
}

int from_bmp_read(FILE* in, struct image* img) {
    enum read_status status = from_bmp(in, img);
    printf("%s", reading_message[status]);
    if (status == 0) {
        return 0;
    } else {
        return 1;
    }
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = create_bmp_header(img);
    fwrite(&header, sizeof(struct bmp_header), 1, out);

    uint8_t padding_number = padding(img->width);

    char* nulls = NULL;
    if (padding_number) {
        nulls = calloc(1, padding_number);
    }

    printf("%u %u %d %lu\n", header.biWidth, header.biHeight, padding_number, sizeof(struct bmp_header));

    for (uint32_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        if (padding_number) fwrite(nulls, 1, padding_number, out);
        if (ferror(out)) {
            free(nulls);
            return WRITE_ERROR;
        }
    }
    free(nulls);
    return WRITE_OK;
}

int to_bmp_write(FILE* out, struct image const* img) {
    enum write_status status = to_bmp(out, img);
    printf("%s", writing_message[status]);
    if (status == 0) {
        return 0;
    } else {
        return 1;
    }
}



