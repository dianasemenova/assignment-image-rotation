#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/rotate_90.h"

int main(int argc, char** argv) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    char* in_path = argv[1];
    char* out_path = argv[2];
    FILE* in = NULL;
    struct image img = {0};

    open_file(in_path, "rb", &in);
    from_bmp_read(in, &img);
    close_file(&in);

    FILE* out = NULL;
    struct image result = rotate (&img);
    open_file(out_path, "wb", &out);
    to_bmp_write(out, &result);
    close_file(&out);

    free(img.data);
    free(result.data);
    return 0;
}


