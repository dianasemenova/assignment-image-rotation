//
// Created by diana on 30.10.22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_90_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_90_H
#include "../include/image.h"
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

struct image rotate( const struct image* orig );

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_90_H



