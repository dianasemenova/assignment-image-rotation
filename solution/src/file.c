//
// Created by diana on 31.10.22.
//
#include "../include/file.h"

const char *opening_message[] = {
        [OPEN_OK] = "File opened successfully\n",
        [OPEN_ERROR] = "Error! File wasn't opened\n"
};

const char *closing_message[] = {
        [CLOSE_OK] = "File closed successfully\n",
        [CLOSE_ERROR] = "Error! File wasn't closed\n"
};

enum open_status file_open_status(char* path, char* type, FILE** file){
    *file = fopen(path, type);
    if (file == NULL){
        return OPEN_ERROR;
    }
    else{
        return OPEN_OK;
    }
}

enum close_status file_close_status(FILE** file){
    if (fclose(*file)==0) {
        return CLOSE_OK;
    }
    else{
        return CLOSE_ERROR;
    }
}

int open_file(char* path, char* type, FILE** file){
    enum open_status status = file_open_status(path, type, file);
    printf("%s", opening_message[status]);
    if (status == 0){
        return 0;
    }
    else{
        return 1;
    }
}

int close_file(FILE** file){
    enum close_status status = file_close_status(file);
    printf("%s", closing_message[status]);
    if (status == 0){
        return 0;
    }
    else{
        return 1;
    }
}



