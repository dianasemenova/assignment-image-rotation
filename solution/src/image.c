//
// Created by diana on 30.10.22.
//
#include "../include/image.h"

struct image image_build(uint64_t width, uint64_t height){
    struct image new_image = {width, height, malloc(height * width * sizeof(struct pixel))};
    return new_image;
}


