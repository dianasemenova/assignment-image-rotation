//
// Created by diana on 30.10.22.
//
#include "../include/rotate_90.h"

struct image rotate(const struct image* orig){
    struct image result = image_build(orig->height, orig->width);
    for (size_t i = 0; i < result.height; i++){
        for (size_t j = 0; j < result.width; j++){
            result.data[i * result.width + j] = orig->data[(orig->height - j - 1) * orig->width + i];
        }
    }
    return result;
}



