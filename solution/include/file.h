//
// Created by diana on 31.10.22.
//
#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum open_status{
    OPEN_OK = 0,
    OPEN_ERROR
};
enum close_status{
    CLOSE_OK = 0,
    CLOSE_ERROR
};
enum open_status file_open_status(char* path, char* type, FILE** file);
enum close_status file_close_status(FILE** file);
int open_file(char* path, char* type, FILE** file);
int close_file(FILE** file);
#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H

