//
// Created by diana on 30.10.22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <malloc.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>


struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel { uint8_t b, g, r; };
struct image image_build(uint64_t width, uint64_t height);
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H




